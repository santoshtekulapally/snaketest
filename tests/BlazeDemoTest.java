import static org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;


import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class BlazeDemoTest {
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
	
		
		System.setProperty("webdriver.chrome.driver","/Users/santoshtekulapally/Desktop/chromedriver");
		  
		driver = new ChromeDriver();
		
		
		driver.get("http://blazedemo.com/");
		
		driver.findElement(By.xpath("//*[@id=\"maincontent\"]/div[2]/div[2]/div/div[1]/div/a")).click();

		
	}

	@After
	public void tearDown() throws Exception {
		
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	public void testdepatures() {
		

		List<WebElement> Selectors = driver.findElements(
				By.cssSelector("table+ul li a"));
				

		System.out.println("Number of links on page: " + Selectors.size());

		
		for (int i = 0; i < Selectors.size(); i++) {
			WebElement link = Selectors.get(i);
			
			
			
		}
		
		// 3. Count the number of options in the selector
		int actualNumLinks = Selectors.size();
		
		// 4. Check that the number of selectors  = 7
		
		assertEquals(7, actualNumLinks);
		
		
	
	}
	@Test
	public void testcasevirginamericaflight() {
		
	
		
         
		
		List<WebElement> actualOutput = driver.findElements(By.cssSelector("table+tr td"));
		
		String actualOutput = outputBox.getText();
		
	
		// 5. Check if actual == expected
		assertEquals("11:23 AM", actualOutput);
		
		// get elements at object 3 
List<WebElement> actualOutput = driver.findElements(By.cssSelector("table+tr td"));
		
		String actualOutput = outputBox.getText();
		
	
		// 5. Check if actual == expected
		assertEquals("11:23 AM", actualOutput);
		
	
		
		//get elements at object 4 
		
List<WebElement> actualOutput = driver.findElements(By.cssSelector("table+tr td"));
		
		String actualOutput = outputBox.getText();
		
	
		// 5. Check if actual == expected
		assertEquals("1:45 PM", actualOutput);
		
		
		 //get elements between the td at object 5 which is cost and compare with the actual output
		
List<WebElement> actualOutput = driver.findElements(By.cssSelector("table+tr td"));
		
		String actualOutput = outputBox.getText();
		
	
		// 5. Check if actual == expected
		assertEquals("$765.32", actualOutput);
		
	}
	
	@Test
	
	public void testcasepurchaseconfoirmation() {
		
		
		// clicks  on the  find flight button 
		WebElement findflight = driver.findElement(
				By.cssSelector("Class#btn btn-primary submit"));
		
		findflight.click();
		
		
		// clicks on the  choose flight button 
		
		WebElement Chooseflightbutton = driver.findElement(
				By.cssSelector("Class#btn btn-small submit"));
		
		Chooseflightbutton.click();
		
		WebElement inputBox = driver.findElement(By.id("inputName"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("Santosh");
		
		WebElement inputBox2 = driver.findElement(By.id("address"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("54 marjory avenue");
		
		WebElement inputBox3 = driver.findElement(By.id("city"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("Toronto");
		
		WebElement inputBox4 = driver.findElement(By.id("state"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("ONTARIO");
		
		WebElement inputBox5 = driver.findElement(By.id("ZipCode"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("M4M2Y4");
		
		WebElement inputBox6 = driver.findElement(By.id("Cardtype"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("credit");
		
		WebElement inputBox7 = driver.findElement(By.id("creditcardnumber"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("99988987876653425");
		
		WebElement inputBox8 = driver.findElement(By.id("month"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("09");
		
		WebElement inputBox9 = driver.findElement(By.id("year"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("2022");
		
		WebElement inputBox10 = driver.findElement(By.id("nameonthecard"));
		// - TYPE name INTO THE BOX
		inputBox.sendKeys("santosh tekulapally");
		
		// 2. Push the button
		
		
		// - GET the button
		
		WebElement purchaseclicked = driver.findElement(
				By.cssSelector("form#value-purchaseflight button"));
		// - PUSH the button
		purchaseclicked.click();
		
		
	
	}

}
